import React from 'react'
import ReactDOM from 'react-dom'
import {inputSnake, Snake} from './snake/snake'
import {Panel} from "./panel";

interface GameProps {
    WidthArea: number
    HeightArea: number
    OneCell: number
}

interface GameState {
    Speed:number
}


class Game extends React.Component<GameProps, GameState>{

    // canvas - Хранится canvas элемент в DOM
    canvas:any

    // snake - объект змейки
    snake:Snake

    // fps - скорость кадров в секунду
    fps:number = 60

    // timerGame - таймер игры
    // отвечает за setInterval который вызывает printer()
    timerGame:any

    // timerSnake - таймер змейки
    // отвечает за шаги змейки или по другому, пересчет змейки
    timerSnake:any

    constructor(props: GameProps) {
        super(props);
        this.snake = this.createNewSnake()
        this.state = {Speed:1}
    }

    //Запуск после рендера документа
    componentDidMount(){
        document.onkeydown = this.onKey.bind(this);

        let canvas:any = document.getElementById('area-game');
        canvas.width = this.props.WidthArea;
        canvas.height = this.props.HeightArea;
        this.canvas = canvas.getContext("2d");
    }

    // Запуск в конце жизненного цикла
    componentWillUnmount(){
        clearInterval(this.timerGame)
        clearInterval(this.timerSnake)
    }

    // createNewSnake() - создание новой змейки
    createNewSnake():Snake{
        let inputsForSnake:inputSnake = {fieldBoundaries: {x:this.props.WidthArea, y:this.props.HeightArea}, sizeCell: this.props.OneCell}
        return new Snake(inputsForSnake)
    }

    // newGame() - старт новой игры
    newGame(){
        this.snake = this.createNewSnake()
        this.timerGame = setInterval(() => this.printer(), Math.trunc(1000 / this.fps));
        this.timerSnake = setInterval(() => this.snakeStep(), Math.trunc(1000 / this.state.Speed));
    }

    // finishGame() - конец игры
    // определяет действи по окончанию игры
    finishGame(){
        clearInterval(this.timerGame)
        clearInterval(this.timerSnake)
        alert("game over")
    }

    // printer() - рисует на canvas
    printer(){
        this.canvas.clearRect(0,0,this.props.WidthArea,this.props.HeightArea);
        // рисуем змею
        for(let i = 0;i<this.snake.body.length;i++)
        {
            this.canvas.fillRect(this.snake.body[i].x, this.snake.body[i].y, this.props.OneCell, this.props.OneCell);
        }
        //рисуем еду
        this.canvas.fillRect(this.snake.eat.x,this.snake.eat.y,this.props.OneCell,this.props.OneCell);
    }

    // snakeStep() - делает шаг змейки
    // и проверяет последсвия шага, умерла ли змейка на этом шаге
    snakeStep() {
        this.snake.ToStep()
        if (this.snake.IsDead()) this.finishGame()
    }

    // onKey - обрботчик нажатия на клавиши
    // принимает на вход событие
    onKey(e:any){
        console.log("key press")
        switch (e.keyCode) {
            // left
            case 37:
                this.snake.ToLeft()
                break;
            // up
            case 38:
                this.snake.ToUp()
                break;
            // right
            case 39:
                this.snake.ToRight()
                break;
            // down
            case 40:
                this.snake.ToDown()
                break;
        }
    }

    reduceSpeed = () => this.setState({Speed:this.state.Speed-0.5})
    increaseSpeed = () => this.setState({Speed:this.state.Speed+0.5})
    restartGame = () => this.newGame()

    render(){
        return <div><canvas width={this.props.WidthArea} height={this.props.HeightArea} id="area-game"></canvas><Panel speed={this.state.Speed} reduceSpeedFunc={this.reduceSpeed.bind(this)} increaseSpeedFunc={this.increaseSpeed.bind(this)} restartGameFunc={this.restartGame.bind(this)}/></div>
    }
}

ReactDOM.render(<Game WidthArea={200} HeightArea={200} OneCell={10} />, document.getElementById('root'))