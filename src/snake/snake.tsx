export interface croods {
    x:number
    y:number
}

export interface inputSnake{
    fieldBoundaries:croods
    sizeCell:number
}

export class Snake{
    // Direction - направление змейки
    // Имеет 4 направлениея
    // d - down -  вниз
    // u - up - вверх
    // l - left - влево
    // r - right - вправо
    private direction: string = "d"

    private sizeCell:number = 1

    // Body - Тело змейки
    // Хранит массив объектов каждый из которых представлет собой координату одной из ячеек
    public body: Array<croods> = [{x:0, y:0}]

    // eat - координаты еды
    public eat:croods = {x:0, y:0}

    // IsLife - Индикатор здоровья змейки
    // false змейка не жива
    // true змейка жива
    private isLife: boolean = true

    // fieldBoundaries - максимальные границы по двум осям
    private fieldBoundaries:croods = {x:1, y:1}

    constructor(inputs:inputSnake) {
        this.fieldBoundaries = inputs.fieldBoundaries
        this.sizeCell = inputs.sizeCell
        this.body.push({x:0,y:this.sizeCell})
        this.generateEat()
    }

    // Изменение направления вниз
    ToDown = () => (this.body[this.body.length-1].y < this.body[this.body.length-2].y) ? false: this.direction = "d"

    // Изменение направления вверх
    ToUp = () => (this.body[this.body.length-1].y > this.body[this.body.length-2].y) ? false: this.direction = "u"

    // Изменение направления влево
    ToLeft = () => (this.body[this.body.length-1].x > this.body[this.body.length-2].x) ? false: this.direction = "l"

    // Изменение направления вправо
    ToRight = () => (this.body[this.body.length-1].x < this.body[this.body.length-2].x) ? false: this.direction = "r"

    // Сделать шаг
    // Пересчитать тело змейки
    ToStep(){
        let head:croods = this.body[this.body.length - 1]
        let newCrood:Object = {}
        switch (this.direction) {
            case "d":
                newCrood = {y: head.y+this.sizeCell}
                break
            case "u":
                newCrood = {y: head.y-this.sizeCell}
                break
            case "l":
                newCrood = {x: head.x-this.sizeCell}
                break
            case "r":
                newCrood = {x: head.x+this.sizeCell}
                break
        }
        return this.IsAction(Object.assign({},head,newCrood))
    }

    // IsDead - мертва ли змейка?
    IsDead = () => !this.isLife

    // IsAction - Действие новой головы
    // Проверяет съела ли змейка еду или здохла
    // Конец игры наступает в одном из следующих случаев:
    // 1 змейка врезалась в границу
    // 2 змейка врезалась в себя
    // Если змейке не хана и она съела еду то добавляет новую голову в тело
    // Если хана то убивает змейку
    private IsAction(newHead:croods){
        // проверка первого варианта
        if(newHead.x >= this.fieldBoundaries.x || newHead.x < 0 || newHead.y >= this.fieldBoundaries.y || newHead.y < 0) return this.isLife = !this.isLife
        // проверка второго варианта
        if(this.isSnake(newHead)) return this.isLife = !this.isLife

        this.body.push(newHead)

        if(this.compareSnakeCroods(newHead,this.eat)){
            this.generateEat()
            return
        }

        return this.body.shift()
    }


    // generateEat - генерация еды
    // Просто перегенирирует еду в новой точке со случайными координатами
    private generateEat(){
        let newEat = this.eat
        while( this.isSnake(newEat) || this.compareSnakeCroods(newEat,{x:0,y:0})){
            newEat = {x:(Math.ceil(Math.random() * (this.fieldBoundaries.x/this.sizeCell)))*this.sizeCell-this.sizeCell, y:(Math.ceil(Math.random() * (this.fieldBoundaries.y/this.sizeCell)))*this.sizeCell-this.sizeCell}
        }
        return this.eat = newEat
    }

    // IsSnake() - проверяет не находится ли переданная координа в змейке
    private isSnake(crood:croods):boolean {
        for (let i = 0; i < this.body.length; i++) {
            if (this.body[i].x == crood.x && this.body[i].y == crood.y) return true
        }
        return false
    }

    // compareSnakeCroods() - Сравнение двух координат
    private compareSnakeCroods(croodOne:croods,croodTwo:croods):boolean{
        if(croodOne.x == croodTwo.x && croodOne.y == croodTwo.y) return true
        else return false
    }

}
