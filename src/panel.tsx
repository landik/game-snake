import React from 'react'

interface panelProps {
    speed:number
    reduceSpeedFunc:any
    increaseSpeedFunc:any
    restartGameFunc:any
}

export function Panel(props:panelProps){
        return <div>
            <div>
                <h3>Скорость:{Math.ceil(props.speed*100)}%</h3>
                <button onClick={props.reduceSpeedFunc}>-50%</button>
                <button onClick={props.increaseSpeedFunc}>+50%</button>
            </div>
            <button onClick={props.restartGameFunc}>Перезапустить игру</button>
        </div>
    }